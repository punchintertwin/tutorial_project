# Tutorial Project

This a starting point to explore the modules 
- [PularDT](https://gitlab.com/ml-ppa/pulsardt.git)
- [PularDT++](https://gitlab.com/ml-ppa/pulsardtpp.git)
- [PulsarRFI_Gen](https://gitlab.com/ml-ppa/pulsarrfi_gen.git) 
- [PulsarRFI_NN](https://gitlab.com/ml-ppa/pulsarrfi_nn.git)

Sample code for initial analysis is provided via Jupyter notebooks, along with some test data. Please look for the examples inside the individual repositories. It is possible to containerize (modified) modules.


## Get started
We provide containerization solution using *Apptainer* (formerly Singularity), 
for information about setting up Apptainer please visit their website and look for the [latest documentation](https://apptainer.org/docs/admin/main/installation.html). The processes described were tested using *Ubuntu 22.04*, 
so we recommend to use this as your operating system. The individual repositories also have Instructions
how to run the code locally, this can also be used on other Operating Systems like Windows or Mac (M1) but 
you might have more work installing necessary dependencies.

### Container library

We provide prebuild container on a public singularity library, which can be pulled with the following command.

```
apptainer pull --arch amd64 library://ml-ppa/pulsardtpp/pulsardtpp_x86_64:latest
```

Change the link to the container of the one you want. Then you can run the container with the command:

```
apptainer run pulsardtpp_x86_64
```

This will start a Jupyter Notebook server from inside the container which has the software modules installed and are usable through the Juypter WebUI. 

To check available builds and choose a container please visit our [library](https://cloud.sylabs.io/library/ml-ppa)


## Building the container

This repository contains the scripts that can be used by Apptainer to build a runnable container out of the submodules. To clone this repository with all its submodules (the flag *--recurse-submodules* is important for this) run the command:

```
git clone --recurse-submodules https://gitlab.com/ml-ppa/tutorial_project.git
cd tutorial_project
```

This generates a directory "tutorial_projects" on your system. 

 Building and running the containers works the same for all modules, the following uses **PulsarDT++** as example. You can build and run the other projects in the same manner just replace the name accordingly. The [pulsardtpp.def](pulsardtpp.def) file contains the definition for the container and can be build using the command:

```
sudo apptainer build pulsardtpp.sif pulsardtpp.def
```

This will generate the image as the pulsardtpp.sif file, with the C++ library being copied, compiled and inserted as an extension into its python instance. It can be started with the command:

```
apptainer run --bind "$PWD" pulsardtpp.sif
```

The container will on start-up run a Jupyter Notebook server which can be accessed through a browser by using the URL output on the terminal. From there, Notebooks outside of the container can be used like the provided **examples**. You can find them in each of the repositories, so for PulsarDT++ you can find them [here](pulsardtpp/examples/). **Make sure that you look for the examples in the project matching the container that you are starting (i.e. you run the container for PulsarDt++ then look for examples in the PulsarDT++ directory)**. Stopping the Jupyter Notebook instance from the browser will also stop the container.

Those steps can be reused for the other packages and their singularity definition files.

## Execution inside a virtual environment

Instructions for local execution/development can be found in the READMEs whithin the individual projects.

## Running Containers on Remote Computing Centers

The goal of this project is to be deployable on remote HPC centers with as few steps as possible. We have tested the deployment with a few different instances so far and have compiled our experiences in the following documents. For Forschungszentrum Jülich, see [here](https://www.punch4nfdi.de/news_amp_events/news/archiv/user_story_running_containers_interactively/) and for the Compute4Punch infrastructure, click [here](https://gitlab-p4n.aip.de/punch/intra-docs-content/-/blob/master/files/TA5/Compute4PUNCH_Access_Usability_JupyterNotebooks.pdf).